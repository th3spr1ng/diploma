# Diploma

Analysis of partial differential equations using Kolmogorov superposition theorem in Hedberg representation

Implemented a new method of analysis partial differential equations, main idea of which is to represent given PDEs as a ordinary diffrential equation using Kolmogorov superposition theorem. We tested this method by running it on 2D Poisson equation and on 2D Bratu’s problem. 